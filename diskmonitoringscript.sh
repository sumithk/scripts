#!/bin/bash
PATH=$PATH:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bon:/root/bin

#+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
#Author : Sumith K
#Date : 14/08/2019
#version 1.0.0
#+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
#Declare Variables
THRESHOLD=80
MAILTO="mailhostingserver@gmail.com"
HOSTNAME=`hostname`
bold=$(tput bold)
normal=$(tput sgr0)
PWD=/root
sendmail=0
#NCDU Package Verification
ncduinstall()
{
cd $PWD
rpm -qa | grep ncdu &> /dev/null
if [ $? -eq 0 ]; then
    echo -e "\nPackage ncdu is already installed and present on the server!" >> diskmonitor.txt
else
    echo -e "\nPackage ncdu is NOT present on the server!.Trying ncdu Installation" >> diskmonitor.txt
        if [ -f /etc/redhat-release ]
            then
              yum -y install ncdu
          echo -e "ncdu Installation completed" >> diskmonitor.txt
            else
             if [ -f /etc/os-release]
             then
              apt-get -y install ncdu
              echo -e "ncdu Installation completed" >> diskmonitor.txt
              else
              echo -e "Could not install the software.Please install the software using the following tuorial \"https://www.tecmint.com/ncdu-a-ncurses-based-disk-usage-analyzer-and-tracker\" to browse and delete unwanted files easily" >> diskmonitor.txt
              fi
         fi
fi
}

#Setup Monitoring Function
adddiskcron()
{
cd $PWD
wget http://142.4.4.187/~hpsscripts/diskmonitoringscript.sh
if [ -f /etc/cron.d/diskmonitor ]
  then
    echo -e "\nDaily Disk monitoring cron is already setup on the server" >> diskmonitor.txt
  else
    echo "* 10 * * * /bin/sh $PWD/diskmonitoringscript.sh" > /etc/cron.d/diskmonitor
    echo -e "\nDaily Disk monitoring Cron has been setup on the file /etc/cron.d/diskmonitor for 10 am server time." >> diskmonitor.txt
  fi
}

#Disk Analysis Function
diskanalysis()
{
cd $PWD
for i in $disk
do
CURRENT=$(df $i | grep / | awk '{ print $5}' | sed 's/%//g')
if [ "$CURRENT" -gt "$THRESHOLD" ]
then
sendmail=1
echo -e "\nYour $i partition is above threshold of $THRESHOLD% and current utilization is $CURRENT%" >> diskmonitor.txt
echo "Top 4 High Usage Directories/Files" >> diskmonitor.txt
du -h --max-depth=1 --exclude=/proc $i/* |sort -rh|head -4 >> diskmonitor.txt
fi
done
ncduinstall
echo -e "\n RUN the command \"ncdu /\" from command line and browse through the directories and remove unwanted files and directories. Please contact your local administrator or technical support if you are doubtful on any system files consuming space." >> diskmonitor.txt
adddiskcron
if [ $sendmail == 1 ]
then
mail -s 'Disk Space Alert' $MAILTO < diskmonitor.txt
fi
}

#Main Code
cd $PWD
echo -e "\n                                   DISK Monitoring Script Report                                 " > diskmonitor.txt
echo -e "\n You are getting this message from the Disk Monitoring Script executed on the server $HOSTNAME" >> diskmonitor.txt
echo -e "\n                                   Disk Space Usage of File System                             " >> diskmonitor.txt
echo -e "\n `df -Th`" >> diskmonitor.txt
echo -e "\n                                       Disk Usage Details                               \n" >> diskmonitor.txt
disk=`df -Th| grep / | awk '{ print $7}'`
echo -e "\n Please analyse your disk usage based on the below results and keep the disk usage under control" >> diskmonitor.txt
diskanalysis
echo -e "\n Please check the file diskmonitor.txt for details if you have not received the mail. Please do check the SPAM folder as well foe the email"
#EOF
