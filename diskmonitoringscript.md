The script is designed to analyze the server and alert over mail if the disk usage has reached the threshold.

By default the threshold is set to 80% and alert goes to mailhostingserver@gmail.com


**Actions Performed in the script
**

1. Check the disk usage of each drive and verify if it exceeds the threshold of 80%
1. Find the 2 high usage directories/files and share over email
1. Check and install ncdu ( Ncurses Disk Usage ) software so that the customers can browse and remove unwanted files and folders
1. Setup a cron to run everyday 10 am server time to monitor the disk usage.


**Values to be changed in the script
**
* Threshold
* MailTo Address


**Steps to Execute the script on a linux server.
**


[root@server ~]# curl http://142.4.4.187/~hpsscripts/diskmonitoringscript.sh | bash



Once the script execution is complete, you will receive a mail on mailhostingserver@gmail.com (Do check SPAM folder as well) with the Disk utilization and high usage directory/files.

                                   DISK Monitoring Script Report                                 

 You are getting this message from the Disk Monitoring Script executed on the server server.demomonkey.org

                                   Disk Space Usage of File System                             

 Filesystem     Type   Size  Used Avail Use% Mounted on
/dev/vda1      ext4   892G  725G  122G  86% /
tmpfs          tmpfs  1.9G     0  1.9G   0% /dev/shm
/dev/vdc       ext4    50G   23G   25G  49% /home3
/usr/tmpDSK    ext3   4.0G  141M  3.6G   4% /tmp
/dev/vde       ext4    50G   13G   34G  28% /test
/dev/vdd1      ext4    50G   23G   25G  49% /checking

                                       Disk Usage Details                               


 Please analyse your disk usage based on the below results and keep the disk usage under control

Your / partition is above threshold of 80% and current utilization is 86%
Top 4 High Usage Directories/Files
535G    //home
255G    //home/downl
166G    //home/hpsscripts
105G    //var

Package ncdu is already installed and present on the server!

 RUN the command "ncdu /" from command line and browse through the directories and remove unwanted files and directories. Please contact your local administrator or technical support if you are doubtful on any system files consuming space.

Daily Disk monitoring Cron has been setup on the file /etc/cron.d/diskmonitor for 10 am server time.